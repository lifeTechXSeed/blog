---
layout: story
title: Ngọn gió đầu mùa - Chapter 1
date: 2020-05-05 22:41:13
tags:
  - first wind
categories:
  - story
---

Đêm nay, đột nhiên gió mùa về, không hề báo trước. Ngay lúc trưa nay vẫn còn nắng rực rỡ rát vàng cả con đường Nguyễn Trãi, hắn mặc chiếc áo cộc tay ngồi trên con dream chiến lên văn phòng. Còn giờ đã phải khoác thêm 1 chiếc áo khoác đi trên đường.

3h đêm, trên con đường bạch mai chỉ còn mập mờ ánh đèn đường cùng với cái lành nhè nhẹ của đầu đông. Hằn đi xe từ từ rẽ vào một con ngõ mập mờ ánh đèn. Đó là quán bún riêu mà hắn hay ăn lúc về đêm như thế này. Khi hắn bước vào đã thấy một đám anh em ngồi sẵn ở đó rồi. Một người trong đám lên tiếng:

\- Anh đến muộn thế anh H ? Bọn em ngồi đây "từ chiều" rồi.

\- T còn phải làm nốt việc trên cty, m lại chém nữa đi SB à đã thấy bán bún đ*o nào lên đâu mà ngồi từ chiều - hắn đáp

SB chỉ còn biết vểnh những chiếc ria mép lên cười trừ. SB là một tên với vẻ ngoài to béo thích để râu, bộ râu kết hợp với bộ mặt của hắn ta kết hợp với nhau tạo ra được một khuôn mặt trông khá bẩn, trái ngược lại so với khuôn mặt tri thức sáng sủa của H. H ngồi xuống cùng bàn với đám ae rồi nói

\- Anh chủ cho như mọi khi đi 

\- H đó hả ? OK nhé 1 bát bún đầy đủ và 1 cốc trà đá đúng không ? - Anh chủ quán đáp lại

\- Vâng anh

Đám anh em đang ngồi đây với hắn phần lớn đều là anh em đồng nghiệp trong cty của hắn. Cả đám đều thường kéo nhau ra đây ăn đêm sau khi làm xong công việc. Hôm nay cũng là một ngày như vậy, chỉ có điều khác là có sự xuất hiện của của tên SB. Hắn quay ra hỏi

\- SB m sắp vào Sài Gòn phải không ?

\- Vâng anh, em sắp vào đấy để thử việc. Hôm nay em ra đây để chia tay anh em trước khi em vào Sài Gòn. Không biết đến bao giờ mới lại được ăn bún cùng anh em

\- M cứ nói như kiểu m sẽ k về Hà Nội nữa vậy. Mấy tháng nữa tết kiểu gì m lại chả ra, còn việc đi với anh em hay không thì phải xem m có sủi k

\- Hà hà - SB lại tiếp tục cười với giọng điệu khá thô bỉ 

Bún cũng đã lên. Giữa trời đêm đông lạnh này, mà được 1 bát bún riêu cua đầy đủ với giò chả, mọi thứ đều nóng hỏi thì phải gọi là hết xẩy. Một quán bùn kỳ lạ chỉ mở vào tầm đêm muộn đến tờ mờ sáng. Nhưng đám người của hắn lại rất thích đến đây ăn, lần nào làm việc xong thậm chí khi rảnh mọi người chơi game với nhau đến đêm xong cũng ra đây ăn. Mọi người vừa ăn vừa nói chuyện trên trời dưới biển. Hắn cảm giác đây là thời khắc thoải mái nhất trong ngày, không phải nghĩ ngợi gì cả, cùng anh em bạn bè tận hưởng sự yên bình trong màn đêm tĩnh lặng. Được 1 lúc, trên bàn đã chỉ còn những cốc trà đà đã vơi đi hơn 1 nửa.

\- Thật sự thời tiết hôm nay như c*t ý, trưa thì đang nóng vkl ra, h thì lại lạnh. May mà t kịp thủ sẵn khoác ở trên văn phòng chứ không h ra chết rét mất - H nói

\- Đông về lạnh thế này giá mà có 1 cô bạn gái để ôm anh H nhỉ ? - SB lại tiếp tục với giọng cười thô bỉ của mình - Sắp đầu 3 rồi đấy anh, anh phải kiếm 1 cô bạn gái cho mình đi chứ, ở vậy 2 mươi mấy năm rồi. Hay là em vào trong đấy kiếm 1 bé trong Sài gòn giới thiệu cho anh, em nghe nói Sài Gòn nhiều gái xinh lại còn giọng nói nghe ngọt lắm chứ

\- Thôi đi m, công việc thì đầy ra đấy, yêu đương cái gì - H đáp lại - Mà t không nghĩ sẽ yêu xa thế làm gì cho khổ đâu, yêu loanh quanh đây là được rồi. Mà thôi cũng gần sáng thôi đi về thôi.

Thế rồi cả đám đứng dậy, rồi ai về nhà nấy. Hắn đi trên đường về và rồi suy nghĩ "yêu đương ư nghe có vẻ xa xỉ, không biết có ai yêu đường một kẻ đến gần sáng mới lên giường đi ngủ như mình không ? Kệ nó đi vậy đến đâu thì đến...". Và rồi trời cũng đã dần sáng, ánh đèn đường dần tắt để nhướng chỗ cho ánh nắng của ban mai, vẫn còn cái lành lạnh của mùa đông nhưng lại có thêm chút nắng ấm, hắn tiếp tục phóng đi trên con đường quen thuộc...

(còn tiếp)
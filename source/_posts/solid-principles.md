---
title: SOLID - nguyên tắc cơ bản của lập trình hướng đối tượng
date: 2020-02-16 20:15:59
author: marverlous
tags:
  - Design pattern
  - Lập trình
  - OOP Programing
categories:
  - Design pattern
---

Đây là bài viết đầu tiên của mình ở blog này. Trước khi bắt đầu vào nội dung chính của bài viết thì mình muốn nói đôi chút về động lực để mình bỏ thời gian dành cho blog này.

- Đầu tiên, mình là một lập trình viên bình thường và mình muốn có thể nâng cao khả năng của mình trở thành một pro. Nên ngoài việc có thể code được cho máy hiểu ra thì cũng phải biết cách trình bày các vấn đề để cho người khác có thể hiểu được. Đây có lẽ cũng là động lực lớn nhất để mình thực hiện viết blog

- Tiếp theo, mình viết ra để học, để ôn lại những kiến thức mà mình đã thu nạp được trong quá trình đọc tài liệu và giải thích lại theo những gì mình thật sự hiểu được. Nên mình cũng rất mong muốn nhận lại những góp ý của các bạn đọc.

- Cuối cùng, là tôi mong những gì tôi viết ra có thể giúp ích được chút xíu cho các bạn đọc trong công việc lập trình

## I. Lập trình hướng đối tượng

Lập trình hướng đối tượng (<i>object orient programming - OOP</i>) là một trong những mô hình được sử dụng phổ biến và hiệu quả rất cao trong quá trình mô hình hóa thế giới thực vào thế giới của những dòng code. Nó có những tính chất như sau:

- Tính trừu tượng (<i>abstraction</i>)
- Tính đóng gói (<i>Encapsulation</i>)
- Tính kế thừa (<i>Inheritance</i>)
- Tính đa hình (<i>Polymorphism</i>)

Nếu đảm bảo được những tính chất trong một project thì ta sẽ nhận được những lợi ích sau:

- Code sẽ dễ đọc, dễ test hơn, maintain dễ dàng hơn

- Đảm bảo được việc tái sử dụng lại code

Tuy nhiên trong quá trình làm việc thì bản thân mình thấy nó sẽ có một vài nhược điểm:

- Để đánh đổi cho việc quá trình maintain và chỉnh sửa trở nên dễ dàng hơn thì việc thiết kế và code ban đầu sẽ mất nhiều thời gian hơn.

- Mặc dù code sẽ dễ đọc hơn nhưng độ phức tạp khi viết code cao hơn, đòi hỏi khả năng sử dụng ngôn ngữ lập trình một cách linh hoạt

  > Cá nhân mình nghĩ nó sẽ không phù hợp với các project ngắn ngày hoặc yêu cầu sự nhanh gọn, đơn giản.

## II. SOLID - 5 nguyên tắc vàng

Mô hình nào cũng vậy, để đảm bảo được các tính chất của nó thì sẽ có những nguyên tắc trong quá trình thiết kế và thực thi. OOP cũng vậy, trải qua biết bao sương máu với những bộ não đầy sạn thì các programmer trên thế giới đã đúc kết được 5 nguyên tắc vàng để có thể đảm bảo 4 tính chất của lập trình hướng đối tượng.

- (S)ingle Responsibility principle
- (O)pen-Closed principle
- (L)iskov subsituation principle
- (I)nterface segregation principle
- (D)ependency inversion principle

> Các ví dụ trong bài viết chủ yếu mình sẽ viết dưới dạng code giả sẽ có hơi hướng của C# để cho các bạn dễ tiếp cận.

### 1. Single reponsibility principle

<b>A class should have one and only one reason to change, meaning that a class should have only one job </b>

Tức là mỗi class chỉ có 1 và chỉ 1 lý do để thay đổi chúng, nói cách khác thì mỗi class chỉ làm một nhiệm vụ.

Xét xem ví dụ dưới đây

```C#
class UserPost {
    void CreatePost(Database db, string postMessange){
        try {
            db.Add(postMessage);
        }
        catch (Exception ex) {
            File.WriteAllText("\\LocalErrors.txt", ex.toString());
        }
    }

    void UpdatePost(Database db, string content){
        try {
            db.Update(content);
        }catch (Exception ex) {
            File.WriteAllText("\\LocalErrors.txt", ex.toString());
        }
    }
}
```

Ở ví dụ trên ta có thể thấy, lớp này có nhiệm vụ chính là quản lý các bài viết của người dùng với 2 phương thức là create và update. Tuy nhiên ta có thể thấy lớp này còn làm việc lưu lại error log ra file điều này là vi phạm với nguyên tắc `single responsibility`. Tức là nếu ta thay đổi cách lưu log ra file thì class này cũng phải thay đổi hay là ta muốn vừa lưu ra file vừa gửi lên 1 server monitor thì sẽ phải sửa hết.

Vậy để đáp ứng được nguyên tắc này code sẽ sửa như sau

```C#
class Logger {
    void error(string errMsg) {
        File.WriteAllText("\\LocalErrors.txt", errMsg);
    }
}
class UserPost {
    private Logger log = new Logger();
    void CreatePost(Database db, string postMessage) {
        try {
            db.Add(postMessage);
        } catch (Exception ex) {
            this.log.error(ex.toString());
        }
    }
}
```

Sau khi chỉnh sửa ta có thể thấy là việc nếu muốn thay đổi cách ghi log ra file thì ta sẽ chỉ cần chỉnh sửa ở lớp `Logger` còn lớp `UserPost` chỉ thay đổi khi ta thay đổi cách quản lý các bài post của người dùng.

> Chú thích: việc tuân thủ nguyên tắc `single responsibility` còn có 1 lợi ích nữa là ta sẽ biết được nhiệm vụ của lớp hay hàm thông qua việc đặt tên mà không cần phải chú thích gì thêm. Điều đó cũng làm cho code trở nên rõ ràng hơn và không có quá nhiều chú thích thừa trong code.

### 2. Open-Closed principle

<b>Objects or entites should be open for extension, but closed for modifications</b>

Phát biểu: Đối tượng hoặc các thực thể có thể mở rộng nhưng sẽ không được phép sửa đổi nó. Tức là nếu ta muốn thêm chức năng vào 1 lớp thì hãy mở rộng nó bằng việc kế thừa thay vì sửa trong class cũ.

Để thấy được lợi ích cụ thể của class này ta xem xét ví dụ dưới đây

```C#
enum Color {Red, Green, Blue};
enum Size {Small, Medium, Large};

struct Product {
    string name;
    Color color;
    Size size;
};

class ProductFilter {
    List<Product> items;

    List<Product> filterByColor(List<Product> items, Color color){
        List<Product> retval = new List<Product>();
        foreach(Product prod in items) {
            if(prod.color == color ) {
                retval.Add(prod);
            }
        }
        return retval;
    }
}
```

Trong ví dụ trên thì lớp `ProductFilter` đã vi phạm vào nguyên tắc `Open-Closed` vì mỗi khi ta thêm một filter vào lớp đó thì đều phải chính sửa trong lớp gốc. Việc này sẽ khiến cho class này rất cồng kềnh và khó kiểm soát. Chưa kể đến nếu thêm thuộc tính vào trong class `Product` để filter nữa thì đúng là một thảm họa. Vì vậy ta có thể sửa lại như sau:

```C#
public class Specifiaction<T>{
    abstract bool isSatified(T item) = 0;
}

public class Filter<T>{
    abstract List<T> filter(List<T> items, Specifiaction<T> spec) = 0;
}

class ProductFilter : Filter<Product> {
    List<Product> filter(List<Product> items, Specifiaction<Product> spec) override {
        List<Product> retval = new List<Product>();
        foreach(Product prod in items) {
            if(spec.isSatified(prod)) {
                retval.Add(prod);
            }
        }
        return retval;
    }
}

class ColorSpecification : Specifiaction<Product> {
    Color color;
    public ColorSpecification(Color color){
        this.color = color;
    }

    bool isSatified(Product item) override {
        return item.color == this.color;
    }
}

public static void Main(){
    Product apple = new Product("Apple", Color.Green, Size.Small);

    List<Product> all = new List<Product>();
    all.Add(apple);

    ProductFilter pfilter = new ProductFilter();
    ColorSpecification green = new ColorSpecification(color Green);

    List<Product> greenThing = pfilter.filter(all, green);
}
```

Sau khi được sửa lại thì ta có thể thấy thay vì việc thay đổi ở class `ProductFilter` mỗi lần thêm một đối tượng hay là filter mới thì ta sẽ thêm filter bằng cách tạo ra 1 class mới kế thừa lại class `Specification`. Như vậy, các class sẽ nhỏ gọn hơn dễ kiểm soát hơn.

### 3. Liskov Subsituation Principle

<b>Subtypes must be subsitutable for their base type</b>

Nguyên tắc này được hiểu như sau các đối tượng (instance) của lớp con sẽ có thể được đại diện bởi các đối tượng của lớp cha mà không gây ra lỗi.

```C#
abstract class Shape {
    abstract float area() = 0;
}

class Square : Shape {
    int size;
    public Square(size) {
        this.size = size;
    }
    float area() override {
        return size * size;
    }
}

void Draw(Shape s){
    Window.Draw(s);
}

public static void Main(){
    Square sq = new Square();
    draw(sq);
}
```

Từ ví dụ trên ta có thể thấy là hàm `draw` có tham số truyền vào là kiểu `Shape`. Nhưng qui tắc này sẽ đảm bảo cho ta là khi ta truyền 1 đối tượng kiểu `Square` là lớp được kế thừa từ kiểu `Shape` thì nó vẫn sẽ không xảy ra lỗi.

### 4. Interface Segregation Principle

<b>A Client should not be forced to implement an interface that is doesn't use</b>

Các lớp dẫn xuất sẽ không cần implement interface mà chúng không cần dùng tới. Tức là thay vì 1 interface có 100 method thì ta hay tách ra thành nhiều interface con với mỗi interface có 1 vài method. Lúc đó lớp dẫn xuất sẽ chỉ cần implement từ những interface nó cần sử dụng đến.

Nguyên tắc này khá đơn giản và nó cũng gần giống với nguyên tắc đầu tiên về tính `single responsibility` bởi vì mục đích cũng là 1 cách để chia 1 interface lớn ra thành nhiều interface con. Nhưng nó sẽ không phải là cách duy nhất.

![interface segregation](/blog/images/interface-segreation.jpg)

### 5. Dependency inversion principle

<b>High-level modules should not depend on low-level modules. Both should depend on abstractions.</b>

<b>Abstractions should not depend on details. Details should depend on abstraction</b>

Nguyên lý này gồm 2 ý chính:

- Các thành phần trong hệ thống sẽ không phụ thuộc lẫn nhau mà sẽ phải phụ thuộc vào các lớp trừu tượng (abstraction)

- Các lớp abstraction sẽ không phụ thuộc vào các lớp cụ thể

Một ví dụ đơn giản ở đây là hệ thống của bạn sẽ có liên kết đến các cơ sở dữ liệu. Tuy nhiên thì sẽ có rất nhiều kiểu cơ sở dữ liệu như MongoDB, MySQL, SQL server,... Thì để tiện lợi cho việc liên kết ta sẽ tạo ra 1 lớp trừu tượng DB. Từng loại DB được implement cụ thể theo đặc điểm của chúng và kế thừa lại lớp trừu tượng đó.

```C#
abstract class Database {
    abstract void Connect() = 0;
    abstract void Disconnect() = 0;
}

class MySQL : Database {
    void Connect() override {

    }

    void Disconnect() override {

    }
}

class Server {
    Database db;
    public Server(Database db) {
        this.db = db;
    }

    public start(){
        this.db.Connect();
    }
}
```

Đây là một nguyên tắc rất quan trọng trong việc cô lập các module với nhau, từ đó có thể tiến hành test các module 1 cách riêng biệt.

## III. Tổng kết

Trên đây là 5 nguyên tắc để thiết kế hướng đối tượng mà giúp cho dự án của bạn có thể đảm bảo được 4 tính chất của mô hình hướng đối tượng. Nếu áp dụng được thành thạo 5 nguyên tắc này sẽ giúp cho việc triển khai dự án dễ dàng hơn.

Tuy nhiên, chúng chỉ là những nguyên tắc tổng quát nhất. Và để đảm bảo được 5 nguyên tắc vào từng vấn đề cụ thể thì người ta đã đúc kết ra được các mẫu thiết kế được gọi là `design pattern` mà mình sẽ đề cập sâu hơn vào các bài viết tiếp theo.

Và mô hình hướng đối tượng này cũng không phải là hoàn hảo và có những nhược điểm nhất định. Hãy cân nhắc kỹ từng vấn để bạn gặp phải cẩn thận trước khi thiết kế để tránh vào `wrost case` của mô hình

## IV. Tham khảo

- [SOLID Principles: A Simple and Easy Explanation](https://hackernoon.com/solid-principles-simple-and-easy-explanation-f57d86c47a7f)

- [DESIGN PATTERNS IN MODERN C++](https://edubookpdf.com/programming/design-patterns-in-modern-c.html)

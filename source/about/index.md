---
title: about
date: 2020-01-21 14:39:43
---

## Tác giả

Căn phòng dưới tháp đồng hồ của một ngôi trường cổ kính nằm giữa lòng thủ đô, là nơi mà 2 đứa nhóc cấp 3 đã gặp nhau và cùng kết hợp với nhau trong một cuộc thi về khoa học. Dù chỉ sinh cách nhau vài tháng nhưng cũng đủ để chia thành khoá trên và khoá dưới. Số phận đã đưa đẩy 2 đứa nhóc đó cùng một đội và đã có 1 khoảng thời gian cùng trốn học, cùng chia nhau từng hộp cơm để mà lăn lội với những bài báo dòng code tưởng chừng như quá to tát với lứa tuổi đó. Sự đam mê công nghệ, thích tìm tòi những vấn đề khó và một vài điểm chung trong tính cách đã làm cho 2 đứa nhóc có 1 sự đồng điệu trong tâm hồn và dần thân thiết với nhau. Để rồi từ đó về sau 2 đứa nhóc đó đã dần gắn bỏ với nhau bởi tình anh em đồng đội đến tận mãi về sau.

Thoáng cái đã 9 năm trôi qua kể từ ngày đó, 2 đứa nhóc giờ đây cũng đã trưởng thành và có cho mình một cuộc sống riêng. Kẻ trời Âu, kẻ đất Việt, nhưng trong thâm tâm chúng vẫn có một dự định ấp ủ với nhau. Blog này sẽ là sự khởi đầu cho các dự án chung của bọn chúng và mong ước hoàn thiện những gì dang dở từ cuộc thi trước kia. Đây là blog viết về công nghệ và những gì bọn chúng trải nhiệm trong cuộc sống dưới cái tên <i style="color: #00B100;">LIFETECH</i>


<table border="0" style="text-align: center; border: none; box-shadow: none;">
 <tr>
    <td border="0" style="padding-right: 75px; border: none">
        <image style="weigth: 250px; height:250px" src="../images/hieunq-ava.jpeg"/>
        <p>Hiếu Ngô (<i>fullstack developer</i>)</p>
        <p>Công nghệ chính: real-time streaming</p>
        <p>Ngôn ngữ lập trình: js, golang, C++, php</p>
    </td>
    <td style="border: none">
        <image style="weigth: 250px; height:250px" src="../images/thu-ava.jpg"/>
        <p>Minh thu (<i>ireland IT master student </i>)</p>
        <p>Công nghệ chính: r</p>
        <p>Ngôn ngữ lập trình: java</p>
    </td>
 </tr>
</table>